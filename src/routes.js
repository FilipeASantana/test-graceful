const { Router } = require('express');
const axios = require('axios');
const logger = require('./logger');

const sleep = ms => new Promise(r => setTimeout(r, ms));

const routes = Router();



routes.get('/', (req, res) => {
    res.json({ message: 'Hello World!' });
});

routes.get('/bad', (req, res) => {
    const e = a + b;

    res.json({ message: 'Bad!' });
});

routes.get('/wait', async (req, res) => {
    let start = new Date();

    let ms = Number(req.query.ms) || 0;

    await sleep(ms);

    let end = new Date();

    res.json({ message: `Waiting for ${ms} milliseconds`, ms, start, end });
    logger.info(`Sleep ${ms} OK`);
});

routes.get('/test', async (req, res) => {
    const start = Date.now();

    const times = [1000, 3000, 2000, 4000, 5000, 6000, 7000, 8000, 9000, 10000, 11000, 12000, 13000, 14000, 15000];
    const api = axios.create({ baseURL: 'http://localhost:8080/wait' });

    const promises = times.map(async ms => {
        await api.get('', { params: { ms } });
    });

    await Promise.all(promises);

    const end = Date.now();
    const ms = end - start;

    res.json({ message: `Test completed in ${ms / 1000} seconds!` });
});



module.exports = routes;
