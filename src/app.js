const express = require('express');
const cors = require('cors');
const pinoHttp = require('pino-http');
const logger = require('./logger');
const routes = require('./routes');



class App {
    port = 8080;

    constructor(port) {
        this.app = express();

        if (port) {
            this.port = port;
        }
    }

    async init() {
        this.app.use(pinoHttp({
            logger,
        }));
        this.app.use(express.json());
        this.app.use(cors());

        this.app.use(routes);

        this.app.use(async (err, req, res, next) => {
            const message500 = 'Internal server error';
            
            const code = err.status || 500;
            const message = err.message || message500;

            logger.error(err);

            const env = process.env.NODE_ENV;

            if (!env || env === 'development') {
                return res.status(code).json(message);
            }

            return res.status(code).json({ code, message: message500 });
        });
    }

    start() {
        this.server = this.app.listen(this.port, () => {
            logger.info(`Server listening on port ${this.port}.`);
        });
    }

    async close() {
        if (this.server && this.server.close) {
            await new Promise((resolve, reject) => {
                this.server.close(err => err ? reject(err) : resolve());
            });
        }
    }
}



module.exports = App;
