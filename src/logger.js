const pino = require('pino');

const env = process.env.NODE_ENV;

const options = !env || env === 'development' ? {
    level: 'debug',
    prettyPrint: {
        levelFirst: true,
        colorize: true,
    },
} : {};

const logger = pino(options);

module.exports = logger;
