const App = require('./app');
const logger = require('./logger');
const graceful = require('./graceful');

graceful(async () => {
    const app = new App(process.env.PORT);
    await app.init();
    app.start();

    return async () => {
        logger.debug('Closing server...');
        await app.close();
        logger.debug('Server closed.');
    };
});
