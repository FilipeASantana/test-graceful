const logger = require('./logger');

const ExitStatus = {
    Failure: 1,
    Success: 0,
};

const exitSignals = ['SIGINT', 'SIGTERM', 'SIGQUIT'];



process.on('unhandledRejection', (reason, promise) => {
    logger.error(
        `App exiting due to an unhandled promise: ${promise} and reason: ${reason}`
    );
    // lets throw the error and let the uncaughtException handle below handle it
    throw reason;
});

process.on('uncaughtException', error => {
    logger.error(`App exiting due to an uncaught exception: ${error}`);
    process.exit(ExitStatus.Failure);
});



const graceful = async handle => {
    try {
        const close = await handle();

        const exitSignalHandler = async exitSignal => {
            try {
                if (close) {
                    await close();
                }

                logger.info(`[${exitSignal}] App exited with success`);
                process.exit(ExitStatus.Success);
            } catch (error) {
                logger.error(`[${exitSignal}] App exited with error: ${error}`);
                process.exit(ExitStatus.Failure);
            }
        };

        for (const exitSignal of exitSignals) {
            process.on(exitSignal, exitSignalHandler);
        }
    } catch (error) {
        logger.error(`App exited with error: ${error}`);
        process.exit(ExitStatus.Failure);
    }
};

module.exports = graceful;
