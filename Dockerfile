FROM node:14-alpine

WORKDIR /app

COPY package.json yarn.lock ./
RUN yarn install --silent && yarn cache clean

COPY . ./

CMD node src/index.js
