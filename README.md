```sh
docker-compose build

# Rode o comando a seguir para desenvolver só no container
mkdir node_modules

# Ou rode o comando a seguir para desenvolver no container e no host
yarn

# Comando para subir o container
docker-compose up -d

# Comando para monitorar os logs da aplicação
docker logs -f test-graceful_app_1

# Comando para parar a execução do container
docker-compose stop

# Comando para excluir o container, imagens e volumes criados
docker-compose down --rmi local -v



# Comando para entrar no container em execução
docker exec -it test-graceful_app_1 sh

# Comando para atualizar as dependêcias no container em execução
docker exec test-graceful_app_1 sh -c 'yarn install --silent && yarn cache clean'
```
